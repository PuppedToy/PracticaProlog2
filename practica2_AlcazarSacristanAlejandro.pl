alumno_prode('Alcazar', 'Sacristan', 'Alejandro', 'w140113').
alumno_prode('Ciudad', 'Sanz', 'Adrian', 'w140292').
alumno_prode('De Lima', 'Rodriguez', 'Guillermo', 'w140091').

sortear(Ini, C, N, P, Fin) :-
	length(Ini, NParticipantes),
	length(Fin, NCamas),
	entre(1, C, N),
	entre(1, NParticipantes, P),
	NuevoP is P-1,
	sortear_(Ini, C, N, NuevoP, Fin, NParticipantes, NCamas).

sortear_(Fin, _C, _N, _P, Fin, _NParticipantes, _NCamas).

sortear_(Ini, C, N, P, Fin, NParticipantes, NCamas) :-
	>(NParticipantes, NCamas),
	NuevaP is mod(-(+(N,P),1), NParticipantes),
	eliminar_elemento(Ini, NuevaP, NuevoIni),
	NuevoNParticipantes is NParticipantes-1,
	sortear_(NuevoIni, C, N, NuevaP, Fin, NuevoNParticipantes, NCamas),
	!.

eliminar_elemento(List, P, ListFin) :- eliminar_elemento_(List, P, ListFin, 0), List \= ListFin.
eliminar_elemento_([], _, [], _).
eliminar_elemento_([_|List], P, ListFin, C) :- P is C, eliminar_elemento_(List, P, ListFin, C+1).
eliminar_elemento_([A|List], P, [A|ListFin], C) :- eliminar_elemento_(List, P, ListFin, C+1).

entre(LimInf, LimSup, N) :-
	integer(LimInf),
	integer(LimSup),
	entre_(LimInf, LimSup, N).

entre_(X, Y, X) :- Y >= X.
entre_(X, Y, Z) :-
	Y > X,
	X1 is X + 1,
	entre_(X1, Y, Z).